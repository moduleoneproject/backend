const TrekEvents = require("../Models/TrekEventsModel");
const mongoose = require("mongoose");

//Get all the events
const getTrekEvents = async (req, res) => {
  const trekEvents = await TrekEvents.find({}).sort({ createdAt: -1 });
  res.status(200).json(trekEvents);
};
//Get events by id
const getTrekEventById = async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).json({ error: "No Such Trek Event invalid id" });
  }
  const trekEvent = await TrekEvents.find({_id:id});

  if (!trekEvent) {
    return res.status(404).json({ error: "No Such Trek Event" });
  }

  res.status(200).json(trekEvent);
};

//Create new event
const createTrekEvent = async (req, res) => {
  try {
    const {
      trekEventId,
      trekEventName,
      trekEventImg,
      trekEventDate,
      trekEventPrice,
    } = req.body;
    if (
      !trekEventId ||
      !trekEventName ||
      !trekEventImg ||
      !trekEventDate ||
      !trekEventPrice
    ) {
      return res
        .status(400)
        .json({ error: "Please fill in all the fields", emptyFields });
    }
    const event = await TrekEvents.create({
      trekEventId,
      trekEventName,
      trekEventImg,
      trekEventDate,
      trekEventPrice,
    });
    res.status(200).json(event);
  } catch (err) {
    res.status(400).json({ error: err.message });
  }
};

//delete a event

const deleteTrekEvent = async (req, res) => {
  try {
    const { id } = req.params;
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return res.status(404).json({ error: "No such Event" });
    }

    const event = await TrekEvents.findOneAndDelete({ _id: id });

    if (!event) {
      return res.status(400).json({ error: "No such workout" });
    }

    res.status(200).json(event);
  } catch (err) {
    res.status(200).send({ error: err.message });
  }
};

//update a event
const updateTrekEvent = async (req, res) => {
  const { id } = req.params;

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(404).json({ error: "No such event" });
  }

  const event = await TrekEvents.updateOne(
    { _id: id },
    {
      ...req.body,
    }
  );

  if (!event) {
    return res.status(400).json({ error: "No such event" });
  }

  res.status(200).json(event);
};

module.exports = {
  getTrekEvents,
  getTrekEventById,
  createTrekEvent,
  deleteTrekEvent,
  updateTrekEvent,
};
