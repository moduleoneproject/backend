const Bookings = require('../Models/BookingModel')
const mongoose = require('mongoose')

//get all bookings
const getBookings = async (req, res) => {
  try{
    const bookings = await Bookings.find({}).sort({ createdAt: -1 });
    res.status(200).json(bookings);
  } catch (error) {
      console.log(error)
  }
}

//post booking data
const eventBooking = async (req, res) => {
    try {
      const {
        trekEventId,
        name,
        email,
        contactNumber
      } = req.body;
      if (
        !trekEventId ||
        !name ||
        !email ||
        !contactNumber
      ) {
        return res
          .status(400)
          .json({ message: "All fields are required"});
      }
      const booked = await Bookings.create({
        trekEventId,
        name,
        email,
        contactNumber
      });
      res.status(200).json(booked);
    } catch (err) {
      res.status(400).json({ error: err.message });
    }
  };

  module.exports = {
    eventBooking,
    getBookings
  }