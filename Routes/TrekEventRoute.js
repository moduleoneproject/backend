const router = require('express').Router()
const {
    getTrekEvents,
    getTrekEventById,
    createTrekEvent,
    deleteTrekEvent,
    updateTrekEvent
} = require('../Controllers/TrekEventController')

router.get('/getTrekEventData', getTrekEvents);
router.get('/getTrekEventData/:id', getTrekEventById);
router.post('/insertTrekEvent', createTrekEvent);
router.put('/updateTrekEvent/:id', updateTrekEvent);
router.delete('/deleteTrekEvent/:id', deleteTrekEvent);


module.exports = router