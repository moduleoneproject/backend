const { Signup, Login } = require('../Controllers/AuthController')
const { UserLogin, UserSignup } = require('../Controllers/UserAuthController')
const { adminVerification } = require('../Middlewares/AuthMiddleware')
const { userVerification } = require('../Middlewares/UserAuthMiddleware')
const router = require('express').Router()

router.post('/user-signup', UserSignup)
router.post('/user-login', UserLogin)
router.post('/signup', Signup)
router.post('/login', Login)
router.post('/',adminVerification)
router.post('/user',userVerification)

module.exports = router