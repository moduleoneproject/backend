const router = require('express').Router()
const {eventBooking, getBookings} = require('../Controllers/BookingController')
router.post('/insertBookingData', eventBooking);
router.get('/getBookings', getBookings)

module.exports = router