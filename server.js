const express = require("express");
const cors = require("cors");
const server = express();
require("dotenv").config();
const { MONGO_URL} = process.env;
const PORT = process.env.PORT || 4000;
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
const authRoute = require("./Routes/AuthRoute");
const trekEventRoute = require("./Routes/TrekEventRoute");
const bookingRoute = require("./Routes/BookingRoute")

mongoose
  .connect(MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("MongoDB is  connected successfully"))
  .catch((err) => console.error(err));

server.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});

server.use(cookieParser());
server.use(express.json());
server.use(
  cors({
    origin: ["http://localhost:3000", "https://main--gentle-cannoli-38a867.netlify.app"],
    methods: ["GET", "POST", "PUT", "DELETE"],
    credentials: true,
  })
);

server.use("/", authRoute);
server.use("/", trekEventRoute);
server.use("/", bookingRoute);
