const mongoose = require("mongoose");

const BookingSchema = new mongoose.Schema({
  trekEventId: {
    type: Number,
    required: [true, "Trek Id is required"],
  },
  name: {
    type: String,
    required: [true, "Your Name is required"],
  },
  email: {
    type: String,
    required: true
  },
  contactNumber: {
    type: Number,
    required: true
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
});


module.exports = mongoose.model("Bookings", BookingSchema);