const mongoose = require("mongoose");

const TrekEventsSchema = new mongoose.Schema({
  trekEventId: {
    type: Number,
    required: [true, "Trek Id is required"],
    unique: true,
  },
  trekEventName: {
    type: String,
    required: [true, "Trek Name is required"],
  },
  trekEventImg: {
    type: String,
    required: [true, "Trek image link is required"],
  },
  trekEventDate: {
    type: String,
    required: [true, "Trek date is required"],
  },
  trekEventPrice: {
    type: Number,
    required: [true, "Price is required"],
  },
  createdAt: {
    type: Date,
    default: new Date(),
  },
});


module.exports = mongoose.model("TrekEvents", TrekEventsSchema);
